package PPT;

import controllers.Controller;
import models.Juego;

/**
 * @author Alexis Eduardo Almanza Ortega 
 * @author Estefany Montaño Estrada
 * @author Juan Montero Terron
 * @author Humberto Antonio Salinas Cortés
 */
public class PPT {

	public static void main(String[] args) {
		Juego juego = new Juego();

		Controller controller = new Controller(juego);

	}

}
