package exceptions;
/**
 * @author Alexis Eduardo Almanza Ortega 
 * @author Estefany Montaño Estrada
 * @author Juan Montero Terron
 * @author Humberto Antonio Salinas Cortés
 */
public class SumaApuestasException extends Exception {
    public SumaApuestasException(String message) {
        super(message);
    }
}
