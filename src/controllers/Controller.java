package controllers;

import gui.VentanaPrincipal;
import models.Juego;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

/**
 * @author Alexis Eduardo Almanza Ortega 
 * @author Estefany Montaño Estrada
 * @author Juan Montero Terron
 * @author Humberto Antonio Salinas Cortés
 *
*/
public class Controller implements ActionListener{
    private VentanaPrincipal ventana;
    private Juego juego;
    public int[] settings;
    public Controller(Juego pJuego) {
        super();
        this.ventana = new VentanaPrincipal(this);
        juego = pJuego;
        ventana.addActionListener(this);
    }

    public void update(int[] settings){
        try{
            juego.setApuestas(settings[0], settings[1], settings[2], settings[3], settings[4]);
        } catch (Exception e){
            JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            ventana.setEdit();
        }
    }

    public void jugar(){
        int [] resultados = juego.nuevaRonda(ventana.getChoice());

        String mensaje = "";
        if(ventana.getChoice() == 0){
            mensaje = "Seleccionaste piedra. \n";
        } else if(ventana.getChoice() == 1){
            mensaje = "Seleccionaste papel. \n";
        } else if(ventana.getChoice() == 2){
            mensaje = "Seleccionaste tijera. \n";
        } else if(ventana.getChoice() == 3){
            mensaje = "Seleccionaste lagarto. \n";
        } else if(ventana.getChoice() == 4){
            mensaje = "Seleccionaste spock. \n";
        }
        if(resultados[1] == 0){
            mensaje += "La maquina selecciono piedra. \n";
        } else if(resultados[1] == 1){
            mensaje += "La maquina selecciono papel.\n";
        } else if(resultados[1] == 2){
            mensaje += "La maquina selecciono tijera.\n";
        }else if(resultados[1]==3) {
        	mensaje += "La maquina selecciono lagarto.\n";
        }else if(resultados[1]==4){
        	mensaje += "La maquina selecciono spock.\n";
        }

        if (resultados[0] == 0){
            mensaje += " Ganaste!!!!\n";
        } else if (resultados[0] == 1){
            mensaje += " Perdiste :(\n";
        } else {
            mensaje += " Es un empate\n";
        }

        mensaje += "Tienes " + juego.getCreditos()+ " creditos";

        ventana.setCreditos(juego.getCreditos());

        JOptionPane.showMessageDialog(null,mensaje, "Resultado", JOptionPane.INFORMATION_MESSAGE);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getActionCommand().equals("Jugar")){
            jugar();
        }
    }
}
