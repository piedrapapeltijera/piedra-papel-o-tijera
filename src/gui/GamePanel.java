package gui;                                                                                    
                                                                                                
import javax.swing.JPanel;                                                                      
import javax.swing.JButton;                                                                     
import javax.swing.JComboBox;                                                                   
                                                                                                
import java.awt.event.ActionEvent;                                                              
import java.awt.event.ActionListener;                                                           
                                                                                                
/**                                                                                             
 * @author Alexis Eduardo Almanza Ortega                                                        
 * @author Estefany Montaño Estrada                                                             
 * @author Juan Montero Terron                                                                  
 * @author Humberto Antonio Salinas Cortés                                                      
 *                                                                                              
*/                                                                                              
public class GamePanel extends JPanel implements ActionListener {                               
    private JComboBox<String> item;                                                             
    private JButton btnClick;                                                                   
    private int choice;  
    private VentanaPrincipal observers[];                                                                      
    public GamePanel() {                                                                        
        super();
        this.observers = new VentanaPrincipal[0];                                                                                
        this.setLayout(null);                                                                   
        //Combo Item                                                                            
	    item=new JComboBox<String>();                                                           
	    item.setBounds(10,50,90,30);                                                            
	    item.addItem("piedra");                                                                 
        item.addItem("papel");                                                                  
        item.addItem("tijera");                                                                 
        item.addItem("lagarto");                                                                
        item.addItem("spock");                                                                  
        add(item);                                                                              
                                                                                                
        btnClick = new JButton( "Jugar" );                                                      
        btnClick.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/play.png")));
	    btnClick.setBounds(10,100,90,30);                                                       
                                                                                                
        addActionListener(this);                                                                
                                                                                                
        add(btnClick);                                                                          
    }                                                                                           
                                                                                                
    public void setEnable(){                                                                    
        btnClick.setEnabled(true);                                                              
    }                                                                                           
                                                                                                
    public void setDisable(){                                                                   
        btnClick.setEnabled(false);                                                             
    }                                                                                           
                                                                                                
    public void addActionListener(ActionListener listener){                                     
        btnClick.addActionListener(listener);                                                   
        item.addActionListener(listener);                                                       
    }                                                                                           
                                                                                                
    public int getChoice(){                                                                     
        return choice;                                                                          
    }                                                                                           
                                                                                                
    private void setChoice(int choice){                                                         
        this.choice = choice;                                                                   
        notifyObservers();
        
    }

    public void addObserver(VentanaPrincipal observer){
        VentanaPrincipal[] temp = new VentanaPrincipal[observers.length + 1];
        for(int i = 0; i < observers.length; i++){
            temp[i] = observers[i];
        }
        temp[observers.length] = observer;
        observers = temp;
    }

    public void notifyObservers(){
        for(int i = 0; i < observers.length; i++){
            observers[i].update();
        }
    }
                                                                                          
                                                                                                
    @Override                                                                                   
    public void actionPerformed(ActionEvent e) {                                                
        if (e.getSource()==item) {                                                              
            setChoice(item.getSelectedIndex());                                                 
        }                                                                                       
    }                                                                                           
}                                                                                               
                                                                                                
                                                                                                
