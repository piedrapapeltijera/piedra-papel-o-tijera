package gui;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JFrame;


import controllers.Controller;

/**
 * @author Alexis Eduardo Almanza Ortega 
 * @author Estefany Montaño Estrada
 * @author Juan Montero Terron
 * @author Humberto Antonio Salinas Cortés
 */
public class VentanaPrincipal extends JFrame implements ActionListener {
	private GamePanel gamePanel;
	private SettingsPanel settingsPanel;
	private int choice;

	public VentanaPrincipal(Controller controller) {
		setLayout(new GridLayout(1, 2));

	    //Tamaño de la ventana
	    setBounds(0,0,550,550);

	    //Titulo
	    setTitle("Piedra,Papel, Tijera, lagarto o Spock");

	    //No redimensionable
	    setResizable(false);

	    //Cerrar proceso al cerrar la ventana
	    setDefaultCloseOperation(EXIT_ON_CLOSE);

		gamePanel = new GamePanel();
		gamePanel.addObserver(this);
		add(gamePanel);

		gamePanel.addActionListener(this);


		settingsPanel = new SettingsPanel(100);
		settingsPanel.addObserver(controller);
		add(settingsPanel);

		settingsPanel.addActionListener(this);
	    //Muestro JFrame (lo �ltimo para que lo pinte todo correctamanete)
	    setVisible(true);
	}

	public void addActionListener(ActionListener listener){
		gamePanel.addActionListener(listener);
		settingsPanel.addActionListener(listener);
	}

	public int[] getSettings(){
		return settingsPanel.getSettings();
	}

	public void setEdit(){
		settingsPanel.setEditar();
		gamePanel.setDisable();
	}

	public int getChoice(){
		return choice;
	}

	public void setCreditos(int creditos){
		settingsPanel.setCreditos(creditos);
	}
	public void update(){
		this.choice = gamePanel.getChoice();
	}

	public void actionPerformed(ActionEvent e) {

		if(e.getActionCommand().equals("0")){
			gamePanel.setDisable();
		}
		if(e.getActionCommand().equals("1")){
			gamePanel.setEnable();
		}
	}
	
}



