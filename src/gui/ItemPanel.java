package gui;

import javax.swing.JPanel;
import java.awt.Component;
import java.awt.GridLayout;

/**
 * @author Alexis Eduardo Almanza Ortega 
 * @author Estefany Montaño Estrada
 * @author Juan Montero Terron
 * @author Humberto Antonio Salinas Cortés
 *
*/

public class ItemPanel extends JPanel {
    public ItemPanel(Component first, Component second, int y) {
        super(new GridLayout(1,2));
        this.add(first);
        this.add(second);
        this.setBounds(0, y, 240, 50);

    }

}
