package gui;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;

import controllers.Controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
/**
 * @author Alexis Eduardo Almanza Ortega 
 * @author Estefany Montaño Estrada
 * @author Juan Montero Terron
 * @author Humberto Antonio Salinas Cortés
 * 
 * Se coloca un 0 como comando de accion al boton editar, y un 1 como comando de accion al boton listo
 */
public class SettingsPanel extends JPanel implements ActionListener{
    private JLabel creditos, piedra, papel, tijera, lagarto, spock;
    private JSpinner piedraSpinner, papelSpinner, tijeraSpinner, lagartoSpinner, spockSpinner;
    private JTextField creditosField;
    private JButton editar, listo;
    private ItemPanel creditosPanel, piedraPanel, papelPanel, tijeraPanel, lagartoPanel, spockPanel;
    private SpinnerNumberModel piedraModel, papelModel, tijeraModel, lagartoModel, spockModel;
    private ImageIcon piedraIcon, papelIcon, tijeraIcon, lagartoIcon, spockIcon;
    private int[] apuestas;
    private Controller[] observers;

    public SettingsPanel(int creditosValue){
        super(); 
        observers = new Controller[0];
        this.setLayout(null);
        this.creditos = new JLabel("Creditos");
        this.creditosField = new JTextField(creditosValue+"");
        this.creditosField.setEditable(false);
        this.creditosPanel = new ItemPanel(creditos, creditosField, 0);

        
        this.add(creditosPanel);


        piedraIcon = new ImageIcon("src/images/piedra.png");

        this.piedra = new JLabel(piedraIcon);
        piedraModel = new SpinnerNumberModel(7, 1, 8, 1);
        this.piedraSpinner = new JSpinner(piedraModel);
        this.piedraSpinner.setEnabled(false);
        this.piedraPanel = new ItemPanel(piedra, piedraSpinner, 70);
        
        this.add(piedraPanel);


        papelIcon = new ImageIcon("src/images/papel.png");

        this.papel = new JLabel(papelIcon);
        papelModel = new SpinnerNumberModel(2, 1, 8, 1);
        this.papelSpinner = new JSpinner(papelModel);
        this.papelSpinner.setEnabled(false);
        this.papelPanel = new ItemPanel(papel, papelSpinner, 140);
        
        this.add(papelPanel);


        tijeraIcon = new ImageIcon("src/images/tijera.png");
        
        this.tijera = new JLabel(tijeraIcon);
        tijeraModel = new SpinnerNumberModel(2, 1, 8, 1);
        this.tijeraSpinner = new JSpinner(tijeraModel);
        this.tijeraSpinner.setEnabled(false);
        this.tijeraPanel = new ItemPanel(tijera, tijeraSpinner, 210);


        this.add(tijeraPanel);
        
        lagartoIcon = new ImageIcon("src/images/lagarto.png");

        this.lagarto = new JLabel(lagartoIcon);
        lagartoModel = new SpinnerNumberModel(2, 1, 8, 1);
        this.lagartoSpinner = new JSpinner(lagartoModel);
        this.lagartoSpinner.setEnabled(false);
        this.lagartoPanel = new ItemPanel(lagarto, lagartoSpinner, 280);
        
        this.add(lagartoPanel);
        
        spockIcon = new ImageIcon("src/images/spock.png");

        this.spock = new JLabel(spockIcon);
        spockModel = new SpinnerNumberModel(2, 1, 8, 1);
        this.spockSpinner = new JSpinner(spockModel);
        this.spockSpinner.setEnabled(false);
        this.spockPanel = new ItemPanel(spock, spockSpinner, 350);
        
        this.add(spockPanel);

        this.editar = new JButton("Editar");
        this.editar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/lapiz.png")));
        this.editar.setActionCommand("0");
        this.listo = new JButton("Listo");
        this.listo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/comprobado.png")));
        this.listo.setActionCommand("1");
        this.listo.setEnabled(false);

        this.addActionListener(this);

        this.add(new ItemPanel(editar, listo, 420));

        
    }

    public void setEditar(){
        this.piedraSpinner.setEnabled(true);
        this.papelSpinner.setEnabled(true);
        this.tijeraSpinner.setEnabled(true);
        this.lagartoSpinner.setEnabled(true);
        this.spockSpinner.setEnabled(true);
        this.listo.setEnabled(true);
        this.editar.setEnabled(false);
    }

    public void addObserver(Controller controller){
        Controller [] temp = observers;
        observers = new Controller[observers.length+1];

        for (int i = 0; i < temp.length; i++) {
            observers[i] = temp[i];
        }
        observers[temp.length] = controller;
    }

    public void updateObservers(){
        for (Controller controller : observers) {
            controller.update(apuestas);
        }
    }

    public void setListo(){
        apuestas = new int[5];
        apuestas[0] = (int) this.piedraSpinner.getValue();
        apuestas[1] = (int) this.papelSpinner.getValue();
        apuestas[2] = (int) this.tijeraSpinner.getValue();
        apuestas[3] = (int) this.lagartoSpinner.getValue();
        apuestas[4] = (int) this.spockSpinner.getValue();
        this.piedraSpinner.setEnabled(false);
        this.papelSpinner.setEnabled(false);
        this.tijeraSpinner.setEnabled(false);
        this.lagartoSpinner.setEnabled(false);
        this.spockSpinner.setEnabled(false);
        this.creditosField.setEditable(false);
        this.listo.setEnabled(false);
        this.editar.setEnabled(true);
        updateObservers();
    }

    public int[] getSettings(){
        return apuestas;
    }

    public void setCreditos(int creditos){
        this.creditosField.setText(creditos+"");
    }

    public void addActionListener(ActionListener listener){
        this.editar.addActionListener(listener);
        this.listo.addActionListener(listener);
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == this.editar){
            this.setEditar();
        }else if(e.getSource() == this.listo){
            this.setListo();
        }
    }
}

