package models;

import exceptions.SumaApuestasException;

/**
 * @author Alexis Eduardo Almanza Ortega 
 * @author Estefany Montaño Estrada
 * @author Juan Montero Terron
 * @author Humberto Antonio Salinas Cortés
 */



public class Juego {
    private int creditos, apuestaPiedra, apuestaPapel, apuestaTijera,apuestaLagarto, apuestaSpock;

    public Juego() {
        creditos = 100;
        apuestaPiedra = 6;
        apuestaPapel = 1;
        apuestaTijera =1;
        apuestaLagarto = 1;
        apuestaSpock =1;
    }
    /**
     * El parametro userItem es un entero que representa la eleccion del usuario
     * 0 = Piedra
     * 1 = Papel
     * 2 = Tijera
     * 3 = Lagarto
     * 4 = Spock
     * 
     * Regresa dos enteros, el primero es un 0 si el usuario gana, 1 si pierde y 2 si empata
     * El segundo es la seleccion del computador.
     * 
     * @return
     *
     * @param userItem
     */

    public int[] nuevaRonda( int userItem ) {

        int gameState = 0;

        int machineItem = (int) (Math.random() * 5);

        if (machineItem == userItem){
            gameState = 2;
            return new int[] { gameState, machineItem };
        }
        System.out.println("userItem: " + userItem);
        switch (userItem) {
            case 0:
                if (machineItem == 2 || machineItem == 3) {
                    creditos += apuestaPiedra;
                    gameState = 0;
                } else {
                    creditos -= apuestaPiedra;
                    gameState = 1;
                }
                break;
            case 1:
                if (machineItem == 0 || machineItem == 4) {
                    creditos += apuestaPapel;
                    gameState = 0;
                } else {
                    creditos -= apuestaPapel;
                    gameState = 1;
                }
                break;
            case 2:
                if (machineItem == 1 || machineItem == 3) {
                    creditos += apuestaTijera;
                    gameState = 0;
                } else {
                    creditos -= apuestaTijera;
                    gameState = 1;
                }
                break;
            case 3:
                if (machineItem == 1 || machineItem == 4) {
                    creditos += apuestaLagarto;
                    gameState = 0;
                } else {
                    creditos -= apuestaLagarto;
                    gameState = 1;
                }
                break;
            case 4:
                if (machineItem == 2 || machineItem == 0) {
                    creditos += apuestaSpock;
                    gameState = 0;
                } else {
                    creditos -= apuestaSpock;
                    gameState = 1;
                }
                break;
        }

        return new int[] { gameState, machineItem };
    }

    public int getCreditos() {
        return creditos;
    }

    public void setApuestas(int apuestaPiedra, int apuestaPapel, int apuestaTijera, int apuestaLagarto, int apuestaSpock) throws SumaApuestasException {
        int sumaApuestas = apuestaPiedra + apuestaPapel + apuestaTijera + apuestaLagarto + apuestaSpock;

        if (sumaApuestas != 15) {
            throw new SumaApuestasException("La suma de las apuestas debe ser 15");
        }

        this.apuestaPiedra = apuestaPiedra;
        this.apuestaPapel = apuestaPapel;
        this.apuestaTijera = apuestaTijera;
        this.apuestaLagarto = apuestaLagarto;
        this.apuestaSpock = apuestaSpock;
    }

}
